﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawn : MonoBehaviour {
	public GameObject spawnSpace;
	public float spawnVelocity;
	public GameObject[] boxes;
	public float spawnHeigt;

	float currentSpawnTime;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		currentSpawnTime += Time.deltaTime;
		if (currentSpawnTime >= spawnVelocity) {
			float spawnPositionX = Random.Range (spawnSpace.transform.position.x - spawnSpace.transform.lossyScale.x / 2,
				                      spawnSpace.transform.position.x + spawnSpace.transform.lossyScale.x / 2);
			float spawnPositionZ = Random.Range (spawnSpace.transform.position.z - spawnSpace.transform.lossyScale.z / 2,
				                      spawnSpace.transform.position.z + spawnSpace.transform.lossyScale.z / 2);

			int boxToInstantiate = Random.Range(0, boxes.Length);
			Vector3 spawnPosition = new Vector3 (spawnPositionX, spawnHeigt, spawnPositionZ);
			Instantiate (boxes [boxToInstantiate], spawnPosition, transform.rotation);

			currentSpawnTime = 0;
		}


	}
}
