﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBifumination : MonoBehaviour {
	public Transform player;

	RaycastHit hitInfo;
	Renderer objectToBifuminate;
	Color originalColor,newColor;
	bool firstPass;
	public float difuminationVelocity;
	float difuminationTime;
	float tempNewColor;
	float tempOriginalColor;
	bool hitSomething;
	bool bifuminationFinished;
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		Bifuminate ();

	}

	void Bifuminate(){
		if (Physics.Linecast (transform.position, player.transform.position, out hitInfo)
			&& hitInfo.collider.tag != "Player") hitSomething = true;
		else hitSomething = false;

		if (hitSomething && firstPass == false) {
			if (firstPass == true) return;
			objectToBifuminate = hitInfo.transform.gameObject.GetComponent<Renderer> ();
			originalColor = objectToBifuminate.material.color;
			newColor = new Color (originalColor.r,originalColor.g,originalColor.b,originalColor.a);
			newColor.a = 0.5f;
			tempNewColor = newColor.a;
			firstPass = true;
		}else if (hitSomething && firstPass == true) {
			if (bifuminationFinished) return;

			print ("Lerp");
			difuminationTime += difuminationVelocity* Time.deltaTime;
			newColor.a = Mathf.Lerp (originalColor.a, tempNewColor, difuminationTime);
			objectToBifuminate.material.color = newColor;

			if (newColor.a== tempNewColor) bifuminationFinished = true;
		} else if (firstPass) {
			objectToBifuminate.material.color = originalColor;
			newColor = originalColor;
			firstPass = false;
			bifuminationFinished = false;
			difuminationTime = 0;
		}
	}
}
